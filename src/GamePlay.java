import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GamePlay extends JPanel implements KeyListener, ActionListener, MouseListener
{
    private boolean isPressed = false;
    private Board board;
    private Boolean pause = false;

    public GamePlay()
    {
        addMouseListener(this);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public void newBoard()
    {
        board = new Board();
    }

    public void ajoutBlockIfIsPressed()
    {
        if(isPressed)
        {
            Point position = Main.fenetre.getMousePosition();
            if(position != null)
            {
                for(int x = 0; x * Main.tailleCellule <= Main.fenetre.getWidth(); x++)
                {
                    for (int y = 0; y * Main.tailleCellule <= Main.fenetre.getHeight(); y++)
                    {
                        String key = x + " " + y;
                        if(position.x >= x * Main.tailleCellule && position.x <= x * Main.tailleCellule + Main.tailleCellule && position.y >= y * Main.tailleCellule && position.y <= y * Main.tailleCellule + Main.tailleCellule)
                        {
                            board.setCellule(key, true);
                        }
                    }
                }
            }
        }
    }

    public void update()
    {
        if(!pause)
        {
            board.nextStep();
        }
    }

    @Override
    public void paint(Graphics g)
    {
        g.setColor(new Color(232, 232, 232));
        g.fillRect(0, 0, Main.fenetre.getWidth(), Main.fenetre.getHeight());
        board.drawCellules(g);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            Main.run = false;
        }
        else if(e.getKeyCode() == KeyEvent.VK_F5)
        {
            board = new Board();
        }
        else if(e.getKeyCode() == KeyEvent.VK_SPACE)
        {
            pause = !pause;
        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_DOWN)
        {
            if(Main.speedExtend + 5 < Main.ticksPerSecond)
            {
                Main.speedExtend += 5;
            }
        }
        else if(e.getKeyCode() == KeyEvent.VK_UP)
        {
            if(Main.speedExtend - 5 > 0)
            {
                Main.speedExtend -= 5;
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        if(e.getButton() == MouseEvent.BUTTON1)
        {
            isPressed = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        if(e.getButton() == MouseEvent.BUTTON1)
        {
            isPressed = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }
}