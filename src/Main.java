import javax.swing.*;
import java.awt.*;

public class Main extends JFrame
{
    public static JFrame fenetre;
    public static boolean run = true;
    public static int tailleCellule = 16;
    public static final GraphicsDevice DEVICE = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

    //framerate
    public static int ticksPerSecond;
    public static int speedExtend = 15;
    private static int skip;
    private static int max;
    private static double next_game_tick;

    public static void main(String[] args) throws InterruptedException
    {
        fenetre = new JFrame();
        GamePlay gamePlay = new GamePlay();
        Image icon = Toolkit.getDefaultToolkit().getImage("logo.png");

        fenetre.setBounds(5, 5, 500, 500);
        fenetre.setTitle("Game of life");
        fenetre.setIconImage(icon);
        fenetre.setResizable(false);
        fenetre.setUndecorated(true);
        fenetre.setVisible(true);
        fenetre.setAlwaysOnTop(true);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.add(gamePlay);
        DEVICE.setFullScreenWindow(fenetre);

        gamePlay.newBoard();
        gamePlay.ajoutBlockIfIsPressed();

        setFramerate(60);
        int loops;
        int count = 0;

        while(run)
        {
            loops = 0;
            while (System.currentTimeMillis() > next_game_tick && loops < max)
            {
                gamePlay.repaint();
                gamePlay.ajoutBlockIfIsPressed();

                if(count % speedExtend == 0)
                {
                    gamePlay.update();
                }
                ++count;

                next_game_tick += skip;
                loops++;
            }
        }
        fenetre.dispose();
    }

    public static void setFramerate(int tps)
    {
        ticksPerSecond = tps;
        skip = 1000 / ticksPerSecond;
        max = 5;
        next_game_tick = System.currentTimeMillis();
    }
}
