import java.awt.*;
import java.util.HashMap;

public class Board
{
    public HashMap<String, Boolean> listeCellule;
    private int xMax;
    private int yMax;

    public Board()
    {
        this.listeCellule = new HashMap<>();

        for(int x = 0; x * Main.tailleCellule <= Main.fenetre.getWidth(); x++)
        {
            for(int y = 0; y * Main.tailleCellule <= Main.fenetre.getHeight(); y++)
            {
                listeCellule.put(x + " " + y, false);
                yMax = y;
            }
            xMax = x;
        }
    }

    public void drawCellules(Graphics g)
    {
        for(int x = 0; x * Main.tailleCellule <= Main.fenetre.getWidth(); x++)
        {
            for(int y = 0; y * Main.tailleCellule <= Main.fenetre.getHeight(); y++)
            {
                if(listeCellule.get(x + " " + y))
                {
                    g.setColor(Color.black);
                }
                else
                {
                    g.setColor(Color.white);
                }
                g.fillRoundRect(x * Main.tailleCellule, y * Main.tailleCellule, Main.tailleCellule, Main.tailleCellule, 100, 100);
            }
        }
    }

    public void setCellule(String key, Boolean state)
    {
        listeCellule.put(key, state);
    }

    public void nextStep()
    {
        HashMap<String, Boolean> newListeCellule = new HashMap<>();

        for(int x = 0; x * Main.tailleCellule <= Main.fenetre.getWidth(); x++)
        {
            for(int y = 0; y * Main.tailleCellule <= Main.fenetre.getHeight(); y++)
            {
                String key = x + " " + y;
                int cellAlive = 0;

                if(x == 0 && y == 0)
                {
                    if(listeCellule.get((x + 1) + " " + y ))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y+1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(x == xMax && y == 0)
                {
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(x == xMax && y == yMax)
                {
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(x == 0 && y == yMax)
                {
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + y))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(x == 0)
                {
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y +1)))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(x == xMax)
                {
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(y ==0)
                {
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + y))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else if(y == yMax)
                {
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y -1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + y))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
                else
                {
                    if(listeCellule.get((x - 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y - 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get(x + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + (y + 1)))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x - 1) + " " + y))
                    {
                        ++cellAlive;
                    }
                    if(listeCellule.get((x + 1) + " " + y))
                    {
                        ++cellAlive;
                    }

                    newListeCellule.put(key, nextStepByAlive(key, cellAlive));
                }
            }
        }

        listeCellule = newListeCellule;
    }

    private boolean nextStepByAlive(String key, int alive)
    {
        boolean res = false;

        if(alive == 3)
        {
            res = true;
        }
        else if(alive == 2)
        {
            res = listeCellule.get(key);
        }

        return res;
    }
}
